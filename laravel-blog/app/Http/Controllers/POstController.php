<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;
class POstController extends Controller
{
    // Controller is for user logout
    public function logout(){
        //it will logout the currently logged in user.
        Auth::logout();
        return redirect('/login');
    }

    //action to return a view containing a form for a blog post creation
    //view - represents view subdirectory under resources folder then views folder 
    public function createPost(){
        return view('posts.create');
    }

    //save content from posts to database
    public function savePost(Request $request){
       //to check whether there is an authenticated user:
       if(Auth::user()){
         //instantiate a new Post Object from the Post method and then save it in a $post variable:
        $post = new Post;

        //define the properties of the $post object using the receieved form data
        $post->title=$request->input('title');
        $post->body=$request->input('content');

        //this will get the id of the authenticated user and set it as the foreign key user_id of the new post.
        $post->user_id=(Auth::user()->id);

        //save the post object in our Post Table;
        $post->save();

        return redirect('/posts');
       }else{
            return redirect('/login');
       }
    }

    //this controller will return all the blog posts
    public function showPosts(){
        //the all() method will save all the records in our Post Table in our $post variable
        $posts = Post::all();

        return view('posts.showPosts')->with('posts', $posts);
    }

    /* Added for Activity s02*/
    public function showFeaturedPosts(){
    $featuredPosts = Post::inRandomOrder()->limit(3)->get(); // Retrieve a random list of 3 posts
    return view('welcome', compact('featuredPosts'));
    }

    //action for showing only the posts authored by authenticated user
    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;
            return view('posts.showPosts')->with('posts', $posts);
        }else{
           return redirect('/login'); 
        }
    }

    //action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    //Added for activity s03
    //action that will allow to find a post using the id passed in via the URL parameter and pass it to an edit form view
    public function editPostForm($id){
        $post = Post::find($id);

        if(!$post){
            return redirect('/posts')->with('error', 'Post not found');
        }return view('posts.edit')->with('post', $post);

    }

    //action that defines the updatePost method to handle the updating of post.
    public function updatePost(Request $request, $id){
    $post = Post::find($id);

    if(!$post){
        return redirect('/posts')->with('error', 'Post not found');
    }

    $post->title = $request->input('title');
    $post->body = $request->input('content');
    $post->save();

    return redirect('/posts')->with('success', 'Post updated successfully');
}

     public function like($id){
            $post = Post::find($id);

            if($post->user_id != Auth::user()->id){

                if($post->likes->contains("user_id", Auth::user()->id)){
                    //delete the like record made by the user before
                    PostLike::where('post_id', $post->id)->where('user_id', Auth::user()->id)->delete();
                }else{
                    //create a new like record to like this post
                    //instantiate a new PostLike object from the Postlike model
                    $postlike = new PostLike;
                    //define the properties of the $postlike
                    $postlike->post_id = $post->id;
                    $postlike->user_id = Auth::user()->id;

                    //save this postlike object in the database
                    $postlike->save();
                }
            }

            return redirect("/posts/$id");
        }

         public function comment(Request $request, $id) {
        $request->validate([
            'content' => 'required',
        ]);

        $postComment = new PostComment;
        $postComment->post_id = $id;
        $postComment->user_id = Auth::user()->id;
        $postComment->body = $request->input('content');
        $postComment->save();

        return redirect("/posts/$id");
    }


}


