<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\POstController;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//This route is for logging out.
Route::get('/logout', [POstController::class, 'logout']);


//This route is for creation of a new post:
Route::get('/posts/create', [POstController::class, 'createPost' ]);

//This route is for saving the post on our database:
Route::post('/posts', [POstController::class, 'savePost']);

//This route is for the list of post on our database:
Route::get('/posts', [POstController::class, 'showPosts']);

// Added for Activity s02
// This route is for displaying featured posts
Route::get('/', [PostController::class, 'showFeaturedPosts'])->name('welcome');

//This route is for defining a route that will return a view containing only the authenticated user's post
Route::get('/myPosts', [POstController::class, 'myPosts']);

//This route will define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [POstController::class, 'show']);

//Added for Activity s03
//This route is for editing post
Route::get('/posts/{id}/edit', [POstController::class, 'editPostForm'])->name('posts.edit');

Route::put('/posts/{id}', [POstController::class, 'updatePost'])->name('posts.update');

//Define a web route that will call the function for liking and unliking a specific post:
Route::put('/posts/{id}/like', [POstController::class, 'like']);

// Comment on a post
Route::post('/posts/{id}/comment', [POstController::class, 'comment']);
