
    @extends("layouts.app")

    @section('tabName')
        Welcome
    @endsection()
    @section('content')

    <!-- Laravel Logo -->
    <div class="logo-container text-center mx-auto mt-4">
        <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="Laravel Logo" class="img-fluid" style="max-width: 300px;">
    </div>

    
    <div class="mt-5">
        <h3 class = "text-center mx-auto">Featured Posts:</h3>
        @if(count($featuredPosts) > 0)
            @foreach($featuredPosts as $featuredPost)
                <div class="card text-center col-3 mx-auto mt-2">
                    <div class="card-body">
                        <h4 class="card-title mb-3">
                            <a href="/posts/{{$featuredPost->id}}">{{$featuredPost->title}}</a></h4>
                        <h6 class="card-text mb-3">Author: {{$featuredPost->user->name}}</h6>
                    </div>
                </div>
            @endforeach
        @else
            <div>
                <h2>No featured posts to show</h2>
            </div>
        @endif
    </div>

@endsection


